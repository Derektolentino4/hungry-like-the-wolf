package Code.simpleOutput;

/**
 * Created by: Hungry like the Wolf Team (Angel, AK, Damaris, and Gloria)
 * Date: 10/29/2019
 * Dance-Off Assignment
 */

import edu.cmu.ri.createlab.terk.robot.finch.Finch;

public class DanceOff
{

   public static void main(final String[] args)
   {
	  // Instantiating the Finch object
      Finch myFinch = new Finch();

      // Set LED Orange and move forward 3 seconds
      myFinch.setLED(255, 165, 0);
      myFinch.setWheelVelocities(255,255,3000);

      // Set LED Purple and back up for 5 seconds
      myFinch.setLED(128, 0, 128);
      myFinch.setWheelVelocities(-255,-255,5000);

      // Set LED Orange, and move forward full speed for 4 seconds
      myFinch.setLED(255, 165, 0);
      myFinch.setWheelVelocities(255,255,4000);

      // Set LED Purple and turn for 4 seconds
      myFinch.setLED(128, 0, 128);
      myFinch.setWheelVelocities(-180,180,4000);

      // Set LED Orange and back up for 5 seconds
      myFinch.setLED(255, 165, 0);
      myFinch.setWheelVelocities(-255,-255,5000);

      // Set LED Purple and turn for a half second
      myFinch.setLED(128, 0, 128);
      myFinch.setWheelVelocities(180,-180,500);

      // Set LED Orange and move forward for a second
      myFinch.setLED(255, 165, 0);
      myFinch.setWheelVelocities(255,255,1000);

      // Set LED Purple and turn for a half second
      myFinch.setLED(128, 0, 128);
      myFinch.setWheelVelocities(180,-180,500);

      // Set LED Orange and move forward for a second
      myFinch.setLED(255, 165, 0);
      myFinch.setWheelVelocities(255,255,1000);

      myFinch.quit();
      System.exit(0);
    }
}

